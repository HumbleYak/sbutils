---Lua Documentation Guide
---https://wiki.eclipse.org/LDT/User_Area/Documentation_Language

---
--@module storyboard
local Storyboard = {}

---
--@type control
local Control = {}

---Wraps the specified control in a #control Lua object.
--@param #string control_name
--@param #string parent_name Layer or Layer.Group
--@return #control control
function Storyboard.wrap_control(control_name, parent_name)
  local new_control = {}
  setmetatable(new_control, {__index=Control})

  new_control.control = control_name
  new_control.parent = parent_name
  
  return new_control
end

---Returns the fully qualified name for this control
--@return #string fqn
function Control:fqn()
  return string.format("%s.%s", self.parent, self.control)
end

---Generates a fully qualified name for a variable on this control
--@param variable (relative variable name)
--@return #string key (fully qualified name)
function Control:key(variable)
  return string.format("%s.%s.%s", self.parent, self.control, variable)
end

---Convenience function for getting the value of a variable on this control
-- e.g. button:get_value("image")
--@param variable (relative variable name)
function Control:get_value(variable)
  return gre.get_value(self:key(variable))
end

---Returns true if hidden, false if not hidden.
--@return #boolean hidden
function Control:is_hidden()
  if(self:get_value("grd_hidden") == 0) then
    return false
  end
  return true
end

---Returns true if visible, false if hidden.
--@return #boolean visible
function Control:is_visible()
  return not self:is_hidden()
end

---Show this control by setting grd_hidden to false
function Control:show()
  return self:set_value("grd_hidden", false)
end

---Hide this control by setting grd_hidden to true
function Control:hide()
  return self:set_value("grd_hidden", true)
end

---Get the value of grd_x for this control 
--@return #number x
function Control:get_x()
  return self:get_value("grd_x")
end

---Get the value of grd_y for this control
--@return #number y
function Control:get_y()
  return self:get_value("grd_y")
end

---Get the value of grd_width for this control
--@return #number width
function Control:get_width()
  return self:get_value("grd_width")
end

---Get the value of grd_height for this control
--@return #number height
function Control:get_height()
  return self:get_value("grd_height")
end

---Get the position of this control grd\_x, grd\_y
--@return #number x
--@return #number y
function Control:get_position()
  return self:get_x(), self:get_y()
end

---Get the size of this control (grd\_width, grd\_height)
--@return #number width
--@return #number height
function Control:get_size()
  local size = gre.get_control_attrs(self:fqn(), "width", "height")
  return size.width, size.height
end

---Get the bounds for this control
--@return #number x
--@return #number y
--@return #number width
--@return #number height
function Control:get_bounds()
  return gre.get_control_attrs(self:fqn(), "x", "y", "width", "height")
end

---Set the value of a variable on this control.
--@param #string variable (relative variable name)
--@param value 
function Control:set_value(variable, value)
  gre.set_value(self:key(variable), value)
end

---Sets this control's hidden state
--@param #boolean hidden
function Control:set_hidden(hidden)
  return self:set_value("grd_hidden", hidden)
end

---Sets this control's hidden state.  Visible is not hidden.  Not visible is hidden.
--@param #boolean visible
function Control:set_visible(visible)
  return self:set_hidden(not visible)
end

---Sets this control's grd_x value
--@param #number x
function Control:set_x(x)
  return self:set_value("grd_x", x)
end

---Sets this control's grd_y value
--@param #number y
function Control:set_y(y)
  return self:set_value("grd_y", y)
end

---Sets this control's grd_width value
--@param #number width
function Control:set_width(width)
  return self:set_value("grd_width", width)
end

---Sets this control's grd_height value
--@param #number height
function Control:set_height(height)
  return self:set_value("grd_height", height)
end

---Sets this control's position (grd\_x, grd\_y)
--@param #number x
--@param #number y
function Control:set_position(x, y)
  return gre.set_control_attrs(self:fqn(), { x = x, y = y })
end

---Sets this control's size (grd\_width, grd\_height)
--@param #number width
--@param #number height
function Control:set_size(width, height)
  return gre.set_control_attrs(self:fqn(), { width = width, height = height })
end

---Sets this control's bounds (grd\_x, grd\_y, grd\_width, grd\_height)
--@param #number x
--@param #number y
--@param #number width
--@param #number height
function Control:set_bounds(bounds)
  return gre.set_control_attrs(self:fqn(), bounds)
end

---Clones this control and returns a wrapped #control representing the newly created control
--@param #string control_name
--@param #string parent_name
--@param #map<#string,#number> data (optional table containing "x", "y", "width", "height", "hidden", "active", "findex"... as used in gre.set\_control\_attrs())
function Control:clone(control_name, parent_name, data)
  gre.clone_object(self:fqn(), control_name, parent_name, data)
  return Storyboard.wrap_control(control_name, parent_name)
end

---This is an extension of #control that is designed to work with a text variable
-- Multiple instances can be used to control different text variables in a single control.
--@type text
--@extends #control
Text = Storyboard.wrap_control()

---Wrap the specified control and text variable in a Lua object
--@param #string control_name
--@param #string parent_name Layer or Layer.Group
--@param #string text variable name
--@return #text Lua object
function Storyboard.wrap_text(control_name, parent_name, variable)
  local new_text = Storyboard.wrap_control(control_name, parent_name)
  setmetatable(new_text, {__index=Text})

  new_text.variable = variable

  return new_text
end

---Sets the value of the text variable for this #text object.
--@param #string text
function Text:set_text(text)
  self:set_value(self.variable, text)
end

---Gets the value of the text variable for this #text object
--@return #string text 
function Text:get_text()
  self:get_value(self.variable)
end

---Formats and sets the value of the text variable for this #text object
--@param format for string.format()
--@param ... arguments for string.format()
function Text:format(format, ...)
  self:set_value(self.variable, string.format(format, unpack(arg)))
end

---This is an extension of #text that always appends the degrees symbol at the end of the text (°).
--@type temptext
--@extends #text
TempText = Storyboard.wrap_text()

---Wrap the specified control and text variable in a Lua object specialized for temperature text.
--@param #string control_name
--@param #string parent_name Layer or Layer.Group
--@param #string text variable name
--@return #temptext
function Storyboard.wrap_temp_text(control_name, parent_name, variable)
  local new_text = Storyboard.wrap_text(control_name, parent_name, variable)
  setmetatable(new_text, {__index=TempText})

  return new_text
end

---Sets the text of this control, formatted as %d°.
--@param #number temp
function TempText:set_temp(temp)
  self:format("%d°", temp)
end

---This is an extension of #control that provides table specific behavior
--@type tablecontrol
--@extends #control
Table = Storyboard.wrap_control()

---Wrap the specified Storyboard table in a Lua object
--@return #tablecontrol
function Storyboard.wrap_table(control_name, parent_name)
  local new_table = Storyboard.wrap_control(control_name,parent_name)
  setmetatable(new_table, {__index=Table})

  return new_table
end

---Creates a key for a cell variable at the specified row and col
--@param #tablecontrol self
--@param #number row
--@param #number col
--@param #string key
function Table:cell_key(row, col, key)
  return string.format("%s.%s.%s.%d.%d", self.parent, self.control, key, row, col)
end

---Gets this table's grd_xoffset
--@return #number xoffset
function Table:get_xoffset()
  return self:get_value("grd_xoffset")
end

---Gets this table's grd_yoffset
--@return #number yoffset
function Table:get_yoffset()
  return self:get_value("grd_yoffset")
end

---Gets the number of rows in this table
--@return #number rows
function Table:get_rows()
  return gre.get_table_attrs(self:fqn(), "rows").rows
end

---Gets the number of cols in this table
--@return #number cols
function Table:get_cols()
  return gre.get_table_attrs(self:fqn(), "cols").cols
end

---Gets the cell height of this table.
--@return #number height
function Table:get_cell_height()
  return gre.get_table_cell_attrs(self:fqn(), 1, 1, "height").height
end

---Gets the cell width of the specified column in this table.  If the col is not specified, then col 1 will be used.
--@param #number col (optional, default 1)
--@return #number width
function Table:get_cell_width(col)
  return gre.get_table_cell_attrs(self:fqn(), 1, col or 1, "width").width
end

---Sets the value of the variable at the specified row and column.
--@param #number row
--@param #number col
--@param #string variable (relative variable name)
--@param value
function Table:set_cell_value(row, col, variable, value)
  gre.set_value(self:cell_key(row, col, variable), value)
end

---Gets the value of the variable at the specified row and column
--@param #number row
--@param #number col
--@param #string variable (relative variable name)
--@return value
function Table:get_cell_value(row, col, variable)
  return gre.get_value(self:cell_key(row, col, variable))
end

return Storyboard
